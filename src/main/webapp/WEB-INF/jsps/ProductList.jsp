<%--
  Created by IntelliJ IDEA.
  User: zzz
  Date: 2016/9/10
  Time: 20:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" +request.getServerPort() + path + "/";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>传智网上商城</title>
    <link href="css/common.css" rel="stylesheet" type="text/css"/>
    <link href="css/product.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="container header">
    <div class="span5">
        <div class="logo">
            <a href="http://localhost:8080/mango/">
                <img src="image/r___________renleipic_01/logo.png" alt="传智播客">
            </a>
        </div>
    </div>
    <div class="span9">
        <div class="headerAd">
            <img src="image/header.jpg" width="320" height="50" alt="正品保障" title="正品保障">
        </div>
    </div>

    <%@include file="menu.jsp"%>

</div>
<div class="container productList">
    <div class="span6">
        <div class="hotProductCategory">
           <c:forEach items="${sessionScope.categorySecondList}" var="category">
                <dl>
                        <dt>
                            <a href="product/product_findByCid?cid=${category.cid}&currentPage=1">${category.cname}</a>
                        </dt>
                    <c:forEach items="${category.categorySecondList}" var="categorySecond">
                        <dd>
                            <a href="product/product_findByCsid?csid=${categorySecond.csid}&currentPage=1">${categorySecond.csname}</a>
                        </dd>
                    </c:forEach>
                </dl>
           </c:forEach>
        </div>
    </div>
    <div class="span18 last">

        <form id="productForm" action="" method="get">
            <input type="hidden" id="brandId" name="brandId" value="">
            <input type="hidden" id="promotionId" name="promotionId" value="">
            <input type="hidden" id="orderType" name="orderType" value="">
            <input type="hidden" id="pageNumber" name="pageNumber" value="1">
            <input type="hidden" id="pageSize" name="pageSize" value="20">

            <div id="result" class="result table clearfix">
                <ul>
                 <c:forEach items="${productPageBean.list}" var="product">
                    <li>
                        <a href="product/product_findByPid?pid=${product.pid}">
                            <img src="${product.image}" width="170" height="170"  style="display: inline-block;">

                            <span style='color:green'>
											 ${product.pname}
											</span>

                            <span class="price">
												商城价： ${product.shop_price}
											</span>
                        </a>
                    </li>
                 </c:forEach>
                </ul>
            </div>

            <div class="pagination">
                <span>第${productPageBean.currentPage}/${productPageBean.totalPage}页</span>

                <c:if test="${productPageBean.currentPage != 1}">
                <a href="product/product_findByCid?cid=${cid}&currentPage=1" class="firstPage"></a>
                <a href="product/product_findByCid?cid=${cid}&currentPage=${productPageBean.currentPage - 1}" class="previousPage"></a>
               </c:if>

                <c:forEach var="i" begin="1" end="${productPageBean.totalPage}">
                   <c:choose>
                       <c:when test="${productPageBean.currentPage != i}">
                          <a href="product/product_findByCid?cid=${cid}&currentPage=${i}">${i}</a>
                       </c:when>
                       <c:otherwise>
                          <span class="currentPage">${i}</span>
                       </c:otherwise>
                   </c:choose>
                </c:forEach>

                <c:if test="${productPageBean.currentPage != productPageBean.totalPage}">
                <a class="nextPage" href="product/product_findByCid?cid=${cid}&currentPage=${productPageBean.currentPage + 1}">&nbsp;</a>
                <a class="lastPage" href="product/product_findByCid?cid=${cid}&currentPage=${productPageBean.totalPage}">&nbsp;</a>
               </c:if>
            </div>

        </form>
    </div>
</div>
<div class="container footer">
    <div class="span24">
        <div class="footerAd">
            <img src="image/footer.jpg" width="950" height="52" alt="我们的优势" title="我们的优势">
        </div>
    </div>
    <div class="span24">
        <ul class="bottomNav">
            <li>
                <a >关于我们</a>
                |
            </li>
            <li>
                <a>联系我们</a>
                |
            </li>
            <li>
                <a >诚聘英才</a>
                |
            </li>
            <li>
                <a >法律声明</a>
                |
            </li>
            <li>
                <a>友情链接</a>
                |
            </li>
            <li>
                <a target="_blank">支付方式</a>
                |
            </li>
            <li>
                <a  target="_blank">配送方式</a>
                |
            </li>
            <li>
                <a >官网</a>
                |
            </li>
            <li>
                <a >论坛</a>

            </li>
        </ul>
    </div>
    <div class="span24">
        <div class="copyright">Copyright©2005-2015 网上商城 版权所有</div>
    </div>
</div>
</body></html>

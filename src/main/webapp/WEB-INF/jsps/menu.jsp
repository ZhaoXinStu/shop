<%--
  Created by IntelliJ IDEA.
  User: zzz
  Date: 2016/9/3
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"     prefix="c"    %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="span10 last">
    <div class="topNav clearfix">
        <ul>
          <c:choose>
            <c:when test="${sessionScope.existUser == 'empty'}">
            <li id="headerLogin" class="headerLogin" style="display: list-item;">
                <a href="${ pageContext.request.contextPath }/user/user_loginPage">登录 </a>|
            </li>
            <li id="headerRegister" class="headerRegister" style="display: list-item;">
                <a href="${ pageContext.request.contextPath }/user/user_registPage">注册</a>|
            </li>
            </c:when>
            <c:otherwise>
                <li id="headerLogin" class="headerLogin" style="display: list-item;">
                    ${sessionScope.existUser.name} |
                </li>
                <li id="headerRegister" class="headerRegister" style="display: list-item;">
                    <a href="#">我的订单</a>|
                </li>
                <li id="headerRegister" class="headerRegister" style="display: list-item;">
                    <a href="${ pageContext.request.contextPath }/user/user_quit">退出</a>|
                </li>
            </c:otherwise>
          </c:choose>
            <li>
                <a>会员中心</a>
                |
            </li>
            <li>
                <a>购物指南</a>
                |
            </li>
            <li>
                <a>关于我们</a>

            </li>
        </ul>
    </div>
    <div class="cart">
        <a  href="./购物车.htm">购物车</a>
    </div>
    <div class="phone">
        客服热线:
        <strong>96008/53277764</strong>
    </div>
</div>
<div class="span24">
    <ul class="mainNav">
        <li>
            <a href="${ pageContext.request.contextPath }/index/execute">首页</a>
            |
        </li>
    <c:forEach items="${sessionScope.categoryList}" var="Category">
        <li>
            <a href="${ pageContext.request.contextPath }/product/product_findByCid?cid=${Category.cid}&currentPage=1">${Category.cname}</a>
            |
        </li>
    </c:forEach>
    </ul>
</div>


</div>
package cn.zx.controller;

import cn.zx.po.Category;
import cn.zx.po.CategoryVO;
import cn.zx.po.Product;
import cn.zx.service.CategoryService;
import cn.zx.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * 首页界面
 * Created by zzz on 2016/9/3.
 */
@Controller
@RequestMapping("/index")
public class IndexController {
    @Autowired
    private CategoryService categoryService; //一级分类模块的service
    @Autowired
    private ProductService productService;   //商品模块的service
    /**
     * 首页界面跳转
     * @return
     */
    @RequestMapping("/execute")
    public ModelAndView showIndexPage(HttpSession session) throws Exception{
        //判断session中用户是否为空，以确定用户是否登录
        if (session.getAttribute("existUser") == null){
            session.setAttribute("existUser", "empty");
        }
        //查询所有的一级菜单,并存入session中
        ArrayList<Category> list =  categoryService.findAll();
        session.setAttribute("categoryList",list);

        /**
         * 查询一级分类对应的二级分类集合
         */
        List<CategoryVO> categorySecondList = categoryService.findFirstAndSecond();
        session.setAttribute("categorySecondList",categorySecondList);

        //创建ModelAndView对象
        ModelAndView modelAndView = new ModelAndView();

        //查询热门商品
        ArrayList<Product> hotProductList = productService.findHotProduct();
        modelAndView.addObject("hotProductList",hotProductList);

       //查询最新商品
        ArrayList<Product> newProductList = productService.finNewProduct();
        modelAndView.addObject("newProductList",newProductList);

        modelAndView.setViewName("index");
        return modelAndView;
    }

}

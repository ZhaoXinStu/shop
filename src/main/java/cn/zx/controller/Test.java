package cn.zx.controller;

import java.util.Scanner;

/**
 * Created by zzz on 2016/9/19.
 */
public class Test {

    public static void main(String[] args) {
        int arr [] = {1,2,3,4,5,6,7,8,9,10};
        Test test = new Test();
        System.out.println(test.find(arr,10));



    }


    public static int find( int arr[] , int val){
        int low = 0;
        int high = arr.length - 1;

        while(low <= high){
            int mid = (high + low) / 2;
            if (arr[mid] == val) {
                return mid;
            }else if (arr[mid] > val){
                high = mid - 1;
            }else {
                low = mid + 1;
            }
        }

        return -1;
    }
}

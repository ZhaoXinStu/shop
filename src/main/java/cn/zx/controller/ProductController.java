package cn.zx.controller;

import cn.zx.po.Category;
import cn.zx.po.CategorySecond;
import cn.zx.po.CategoryVO;
import cn.zx.po.Product;
import cn.zx.service.CategorySecondService;
import cn.zx.service.CategoryService;
import cn.zx.service.ProductService;
import cn.zx.utils.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 商品模块的Controller
 * Created by zzz on 2016/9/8.
 */
@Controller
@Transactional
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService; //商品模块的Service
    @Autowired
    private CategoryService categoryService; //一级分类的Servcie
    @Autowired
    private CategorySecondService categorySecondService;// 二级分类的
    /**
     * 根据相应PID，查询相应的商品详细信息
     * @param pid
     * @return
     */
    @RequestMapping("/product_findByPid")
    public ModelAndView findByPid(@RequestParam(value = "pid") Integer pid) throws Exception{

        Product product = productService.findByPid(pid);
        System.out.println(product +" : "+ pid);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("product",product);
        modelAndView.setViewName("product");
        return modelAndView;
    }

    /**
     * 按照cid并且分页查询该一级分类下的所有商品信息
     * @return
     */
    @RequestMapping("/product_findByCid")
    public ModelAndView findByCid(@RequestParam(value = "cid") Integer cid,@RequestParam(value = "currentPage") Integer currentPage) throws Exception{


        /**
         * 按照cid并且按页查询该一级分类下的所有商品信息，
         */
        PageBean<Product> productPageBean = productService.findByPageCid(cid,currentPage);


        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("productPageBean",productPageBean);
        modelAndView.addObject("cid",cid);
        modelAndView.setViewName("ProductList");
        return modelAndView;
    }

    /**
     * 查询二级菜单商品
     */
    @RequestMapping("/product_findByCsid")
    public ModelAndView findByCsid(@RequestParam (value = "csid") Integer csid,@RequestParam( value = "currentPage") Integer currentPage)throws Exception{
            //查询所有的二级分类下的商品
            PageBean<Product> productPageBean = productService.findByCsid(csid, currentPage);


            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("productPageBean",productPageBean);
            modelAndView.setViewName("ProductList");
            return modelAndView;
    }





}

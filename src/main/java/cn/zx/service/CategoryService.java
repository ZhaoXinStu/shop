package cn.zx.service;

import cn.zx.po.Category;
import cn.zx.po.CategoryVO;

import java.util.ArrayList;
import java.util.List;

/**
 * 一级分类菜单的service层接口
 * Created by zzz on 2016/9/8.
 */
public interface CategoryService {
    /**
     * 查询所有的一级分类商品
     * @return
     */
    ArrayList<Category> findAll() throws Exception;

    /**
     * 查找一级菜单和与之匹配的二级菜单
     * @return
     * @throws Exception
     */
    List<CategoryVO> findFirstAndSecond() throws Exception;
}

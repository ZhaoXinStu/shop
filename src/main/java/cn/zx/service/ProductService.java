package cn.zx.service;

import cn.zx.po.Product;
import cn.zx.utils.PageBean;

import java.util.ArrayList;

/**
 * 商品模块的servcie接口
 * Created by zzz on 2016/9/8.
 */
public interface ProductService {

    /**
     * 查询热门商品
     * @return
     * @throws Exception
     */
    ArrayList<Product> findHotProduct()throws Exception;

    /**
     * 查询最新商品
     * @return
     * @throws Exception
     */
    ArrayList<Product> finNewProduct() throws Exception;

    /**
     * 根据相应PID，查询相应的商品详细信息
     * @return
     * @throws Exception
     */
    Product findByPid(Integer pid) throws Exception;

    /**
     * 按照cid并且按页查询该一级分类下的所有商品，
     * @param cid
     * @param currentPage
     * @return
     */
    PageBean<Product> findByPageCid(Integer cid, Integer currentPage) throws Exception;

    /**
     * 按照二级分类的ID查询所属的所有的商品
     * @param csid
     * @param currentPage
     * @return
     */
    PageBean<Product> findByCsid(Integer csid, Integer currentPage) throws Exception;
}

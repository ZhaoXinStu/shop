package cn.zx.service;

import cn.zx.mapper.UserMapper;
import cn.zx.po.User;
import org.springframework.stereotype.Service;

/**
 * 用户模块的业务逻辑层接口
 * Created by zzz on 2016/9/3.
 */
public interface UserService {
    /**
     * 检查该用户，是否已经存在
     */
     User findByUsername(String username) throws Exception;

    /**
     * 用户注册，保存用户信息
     * @param user
     */
     void saveUserInfo(User user) throws Exception;

    /**
     * 用户激活
     * @param code
     * @return
     */
    User findByCode(String code)throws Exception;

    /**
     * 修改数据库中保存的用户信息
     * @param existUser
     * @throws Exception
     */
    void updateUserInfo(User existUser) throws Exception;

    /**
     *查找数据库，用户登录
     * @param user
     * @return
     * @throws Exception
     */
    User login(User user) throws Exception;
}

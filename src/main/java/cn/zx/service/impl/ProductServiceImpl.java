package cn.zx.service.impl;

import cn.zx.mapper.ProductMapper;
import cn.zx.po.Product;
import cn.zx.service.ProductService;
import cn.zx.utils.PageBean;
import org.apache.poi.common.usermodel.LineStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品模块的service层实现类
 * Created by zzz on 2016/9/8.
 */
@Service("productServiceImpl")
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper;

    /**
     * 查询热门商品
     * @return
     * @throws Exception
     */
    public ArrayList<Product> findHotProduct() throws Exception {
       ArrayList<Product> hotProductList = productMapper.findHotProduct();

        return hotProductList;
    }

    /**
     * 查询最新商品
     * @return
     * @throws Exception
     */
    public ArrayList<Product> finNewProduct() throws Exception {
        ArrayList<Product> newProductList = productMapper.findNewProduct();
        return newProductList;
    }

    /**
     * 根据相应PID，查询相应的商品详细信息
     * @return
     * @throws Exception
     */
    public Product findByPid(Integer pid) throws Exception {
        Product product = productMapper.findByPid(pid);
        return product;
    }

    /**
     * 按照cid并且按页查询该一级分类下的所有商品信息
     * @param cid
     * @param currentPage
     * @return
     * @throws Exception
     */
    public PageBean<Product> findByPageCid(Integer cid, Integer currentPage) throws Exception {
        PageBean<Product> productPageBean = new PageBean<Product>();
        //设置当前页数
        productPageBean.setCurrentPage(currentPage);
        //设置每页显示记录数
        int limit = 12;
        productPageBean.setLimitPage(limit);
        //设置总记录数
        int totalCount = 0;
        totalCount = productMapper.findCountByCid(cid);  //按照id查找指定一级分类下所有商品的记录数
        productPageBean.setTotalCount(totalCount);
        //设置总页数
        int totalPage = 0;
        totalPage = (int)Math.ceil(totalCount / limit); //向上取整

        if (totalPage == 0) {
            totalPage = totalPage + 1;  //为了防止总页数为0的出现
        }

        productPageBean.setTotalPage(totalPage);
        //设置每页显示的集合数据
        int begin = (currentPage - 1) * limit;
        List<Product> list = productMapper.findProductListByCid(cid,begin,limit); //按照cid，查找指定一级分类下的所有的商品的集合
        productPageBean.setList(list);

        return productPageBean;
    }

    /**
     * 按照二级分类的ID，查找所属的所属的所有商品
     * @param csid
     * @param currentPage
     * @return
     * @throws Exception
     */
    public PageBean<Product> findByCsid(Integer csid, Integer currentPage) throws Exception {
        PageBean<Product> productPageBean = new PageBean<Product>();  //设置pageBean类
        productPageBean.setCurrentPage(currentPage);  //设置当前页数
        int limit = 12;
        productPageBean.setLimitPage(limit); //设置每页所能的商品个数
        int totalCount = 0;
        totalCount = productMapper.findCountByCsid(csid);  //查询二级分类所属的商品总个数
        productPageBean.setTotalCount(totalCount);   //设置商品总数

        int totalPage = (int) Math.ceil(totalCount / limit);  //计算商品的总页数

        if (totalPage == 0) {
            totalPage = totalPage + 1;  //为了防止总页数为0的出现
        }

        productPageBean.setTotalPage(totalPage); //设置商品的总页数

        int begin = (currentPage - 1) * limit;
        List<Product> productList = productMapper.findProductListByCsid(csid, begin, limit); //查询二级分类所属的每页商品
        productPageBean.setList(productList); //将商品集合注入

        return productPageBean;
    }
}

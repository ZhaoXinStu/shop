package cn.zx.service.impl;

import cn.zx.mapper.CategoryMapper;
import cn.zx.po.Category;
import cn.zx.po.CategoryVO;
import cn.zx.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 一级分类菜单的service层实现类
 * Created by zzz on 2016/9/8.
 */
@Service("categoryServiceImpl")
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 查询所有的一级分类商品
     * @return
     * @throws Exception
     */
    public ArrayList<Category> findAll() throws Exception {
        ArrayList<Category> list = categoryMapper.findAll();
        return list;
    }

    /**
     * 查找一级菜单和与之匹配的二级菜单
     * @return
     * @throws Exception
     */
    public List<CategoryVO> findFirstAndSecond() throws Exception {
        ArrayList<CategoryVO> list = categoryMapper.findFirstAndSecond();
        return list;
    }
}

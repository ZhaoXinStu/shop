package cn.zx.mapper;

import cn.zx.po.Product;
import cn.zx.utils.PageBean;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品模块的dao层接口
 * Created by zzz on 2016/9/8.
 */
public interface ProductMapper {

    /**
     * 查询热门商品
     * @return
     * @throws Exception
     */
    ArrayList<Product> findHotProduct() throws Exception;

    /**
     * 查询最新商品
     * @return
     * @throws Exception
     */
    ArrayList<Product> findNewProduct() throws Exception;

    /**
     * 根据相应PID，查询相应的商品详细信息
     * @param pid
     * @return
     * @throws Exception
     */
    Product findByPid(Integer pid) throws Exception;



    /**
     * 按照cid并且按页查询该一级分类下的所有商品记录数
     * @param cid
     * @return
     */
    int findCountByCid(Integer cid) throws Exception;

    /**
     * 按照cid，查找指定一级分类下的指定数量的商品集合
     * @param cid 一级分类的cid
     * @param begin  //查找开始的商品序号
     * @param limit  //每页查找的商品数量
     * @return
     * @throws Exception
     */
    List<Product> findProductListByCid(@Param("cid") Integer cid, @Param("begin") Integer begin, @Param("limit") Integer limit) throws Exception;

    /**
     * 根据二级分类的ID，查询其所属的商品总个数
     * @return
     */
    int findCountByCsid(Integer csid) throws Exception;

    /**
     * 根据二级分类的ID，查询每页的商品集合
     * @param csid
     * @param begin
     * @param limit
     * @return
     */
    List<Product> findProductListByCsid(@Param("csid") Integer csid, @Param("begin") Integer begin, @Param("limit") Integer limit) throws Exception;
}

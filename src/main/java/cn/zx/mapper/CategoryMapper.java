package cn.zx.mapper;

import cn.zx.po.Category;
import cn.zx.po.CategoryVO;

import java.util.ArrayList;

/**
 * 一级分类菜单的dao层接口
 * Created by zzz on 2016/9/8.
 */
public interface CategoryMapper {

    /**
     * 查询所有的一级分类商品
     * @return
     */
    ArrayList<Category> findAll() throws Exception;

    /**
     * 查询所有的一级菜单和与之匹配的二级菜单
     * @return
     * @throws Exception
     */
    ArrayList<CategoryVO> findFirstAndSecond() throws Exception;
}

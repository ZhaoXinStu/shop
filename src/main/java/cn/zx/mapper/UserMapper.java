package cn.zx.mapper;

import cn.zx.po.User;

/**
 * 用户模块的dao层接口
 * Created by zzz on 2016/9/4.
 */
public interface UserMapper {

    /**
     * 检验该用户是否已存在
     * @return
     */
    User findByUsername(String username) throws Exception;

    /**
     * 用户注册，保存用户信息
     * @param user
     */
    void saveUserInfo(User user) throws Exception;

    /**
     * 用户激活
     * @param code
     * @return
     */
    User findByCode(String code) throws Exception;

    /**
     * 修改数据库中保存的用户信息
     * @param existUser
     * @throws Exception
     */
    void updateUserInfo(User existUser) throws Exception;

    /**
     * 查找数据库，用户登录
     * @param user
     * @return
     * @throws Exception
     */
    User login(User user) throws Exception;
}

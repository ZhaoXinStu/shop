package cn.zx.utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * 发送邮件的工具类
 * Created by zzz on 2016/9/6.
 */
public class MailUtils {
    /**
     * 发送有邮件的方法
     * @param to     : 收件人
     * @param code   ：激活码
     */
    public static void sendMail(String to, String code){
        /**
         * 主要有3个对象
         * 1.获得一个session对象
         * 2.创建一个代表邮件的对象： Message对象
         * 3.发送邮件对象： Transport对象
         */

        //1.获得连接对象
        Properties properties = new Properties();
        properties.setProperty("mail.host", "localhost");
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("Stu@shop.com","123");
            }
        });

        //2.创建邮件对象
        Message message = new MimeMessage(session);
        try {
            //设置发件人
            message.setFrom(new InternetAddress("Stu@shop.com"));
            //设置收件人,CC:抄送，密送：BCC
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //设置标题
            message.setSubject("来自西安邮电集团科技分公司购物网站官方用户激活注册码");
            //设置正文,链接:text/html,  纯文本的：text:plain
            message.setContent("<h1>购物网站的官方激活码：点下面的链接完成激活操作!</h1><h3><a href='http://192.168.1.110:8080/user/user_active?code="+ code +"'>http://192.168.1.110:8080/user/user_active?code="+ code +"</a></h3>", "text/html;charset=UTF-8");
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        //3.发送邮件
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /*public static void main(String[] args) {
        sendMail("13259426521@163.com","1111111111");
    }*/

}

package cn.zx.utils;

import java.util.UUID;

/**
 * 生成随机字符串的工具类
 * Created by zzz on 2016/9/5.
 */
public class UUIDUtils {

    /**
     * 获得随机的字符串
     * @return
     */
    public static String  getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }



}

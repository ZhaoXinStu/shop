package cn.zx.utils;

import java.util.List;

/**
 * 数据分页Bean
 * Created by zzz on 2016/9/10.
 */
public class PageBean<T> {
    private int currentPage; // 当前页数
    private int totalCount; //总记录数
    private int totalPage;  //总页数
    private int limitPage;  //每页显示的数据个数
    private List<T> list;   //每页显示数据的集合


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getLimitPage() {
        return limitPage;
    }

    public void setLimitPage(int limitPage) {
        this.limitPage = limitPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "currentPage=" + currentPage +
                ", totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", limitPage=" + limitPage +
                ", list=" + list +
                '}';
    }
}

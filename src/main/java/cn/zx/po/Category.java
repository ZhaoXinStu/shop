package cn.zx.po;

import java.util.ArrayList;

/**
 * 一级分类菜单的pojo
 * Created by zzz on 2016/9/8.
 */
public class Category {
    private Integer cid;
    private String cname;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }


}

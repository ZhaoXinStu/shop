package cn.zx.po;

/**
 * 购物项对象，某个商品的信息
 * Created by zzz on 2016/9/17.
 */
public class CartItem {

    private Product product; //购物项中的商品信息
    private int count;       //某种商品的数量
    private double subtotal; //该种商品的小结

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 小计，计算得出
     * @return
     */
    public double getSubtotal() {
        return count*product.getShop_price();
    }


}

package cn.zx.po;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 封装购物车对象
 * Created by zzz on 2016/9/17.
 */
public class Cart {
    //购物项目集合:map 中的key是商品的id，value就是购物项信息
    private Map<Integer,CartItem> map = new LinkedHashMap<Integer, CartItem>();
    //购物的总计
    private double total;

    //为了界面上可以获取总计
    public double getTotal() {
        return total;
    }

    //为了遍历购物车方便，提供的方法，将map中的value取出来
    public Collection<CartItem> getCartItems(){
        return map.values();
    }



    /*购物车的功能*/
    //1.将购物项添加到购物车中
    public void addCart(CartItem cartItem){
        Integer pid = cartItem.getProduct().getPid(); //获取商品的PID
        /**
         *  1.如果存在
         *         数量增加
         *
         *  2.如果不存在
         *         添加购物项
         */
        if (map.containsKey(pid)) {
                CartItem cartItem1 = map.get(pid);
                cartItem1.setCount(cartItem.getCount() + cartItem1.getCount());
        }else{
            map.put(pid, cartItem);
        }

        //设置总计
        total += cartItem.getSubtotal();
    }

    //2.移除购物车中的购物项
    public void remove(Integer pid){
        CartItem cartItem = map.remove(pid);
        total -= cartItem.getSubtotal();
    }

    //3.清空购物车
    public void clearCart(){
        map.clear(); //清空购物车
        total = 0; //将商品的总计清零
    }

}

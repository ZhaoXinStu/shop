package cn.zx.po;

import java.util.List;

/**
 * 一级分类模块和二级分类模块
 * Created by zzz on 2016/9/10.
 */
public class CategoryVO {
    private Integer cid;
    private String cname;
    private List<CategorySecond> categorySecondList;

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public List<CategorySecond> getCategorySecondList() {
        return categorySecondList;
    }

    public void setCategorySecondList(List<CategorySecond> categorySecondList) {
        this.categorySecondList = categorySecondList;
    }


    @Override
    public String toString() {
        return "CategoryVO{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", categorySecondList=" + categorySecondList +
                '}';
    }
}
